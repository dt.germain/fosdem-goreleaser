#!/bin/bash
TAG=$1
git add .
git commit -m "$TAG"
git tag -a $TAG -m "$TAG"
git push origin $TAG
