#!/bin/bash

cp .Dockerfile.makefile Dockerfile
rm .goreleaser.yaml

git add .
git commit -m "Initial commit"
git push origin main --force

git push --delete origin 0.0.1
git push --delete origin 0.0.2
git push --delete origin 0.0.3
git push --delete origin 0.0.4
git tag -d 0.0.1
git tag -d 0.0.2
git tag -d 0.0.3
git tag -d 0.0.4

echo "don't forget to clean the previous releases !!"
