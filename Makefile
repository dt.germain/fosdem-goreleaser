prepare:
	go mod tidy

build: prepare
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/tifling -ldflags "-X main.Version=$$VERSION" main.go

dockerbuild:
	docker build -t zwindler/tifling:$$VERSION --build-arg VERSION=$$VERSION . && docker build -t zwindler/tifling:latest --build-arg VERSION=$$VERSION .

dockerpush: dockerbuild
	docker push zwindler/tifling:$$VERSION && docker push zwindler/tifling:latest
